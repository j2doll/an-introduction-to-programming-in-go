* go version  
- `go version go1.4.2 linux/amd64`
- go version go1.11.2 windows/amd64(2018) with Visual Studio Code

* go help  
The Go tool suite is made up of several different commands and sub-commands. A list of those commands is available by using *go help*
